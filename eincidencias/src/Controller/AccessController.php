<?php
namespace Drupal\eincidencias\Controller;

use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Session\AccountProxyInterface;

class AccessController implements AccessInterface {
  
  /**
   * Manage permissions for development content.
   * @param AccountInterface $account
   * @param String $operation
   * @param NodeInterface $node
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function developmentAccess(AccountInterface $account, String $operation, NodeInterface $node = null) {
    switch($operation) {
      case 'add':
        return AccessResult::allowedIf($account->hasPermission('add development'));
        break;
      case 'alter':
      case 'access':
        if (isset($node) && ($node->getType() == 'development') && $node->isPublished()) {
          return AccessResult::allowedIf($account->hasPermission('alter all developments'));
        }
        break;
      case 'add-user':
        if (isset($node) && ($node->getType() == 'development') && $node->isPublished()) {
          return AccessResult::allowedIf($account->hasPermission('add customer all developments'));
        }
        break;
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage permissions for dashboard access.
   * @param AccountInterface $account
   * @param UserInterface $user
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function dashboardAccess(AccountInterface $account, UserInterface $user) {
    if (isset($user)) {
      if (
          ($account->id() == $user->id()) && (
              $user->hasRole('eincidencias_customer') ||
              $user->hasRole('eincidencias_technician') ||
              $user->hasRole('eincidencias_outsource')
            )
          )
      {
        return AccessResult::allowedIfHasPermission($account, 'access own dashboard');
      } else {
        return AccessResult::allowedIfHasPermission($account, 'access all dashboards');
      }
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage Add incident permissions
   * @param AccountInterface $account
   * @param string $operation
   * @param UserInterface $user
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function incidentAddAccess(AccountInterface $account, string $operation, UserInterface $user) {
    if (isset($user) && ($operation == 'add') && ($account->id() == $user->id()) && $user->hasRole('eincidencias_customer')) {
      return AccessResult::allowedIfHasPermission($account, 'add incident own develop');
    } else {
      return AccessResult::allowedIfHasPermission($account, 'add incident all developments');
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage list incidents
   * @param AccountInterface $account
   * @param UserInterface $user
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function incidentListAccess(AccountInterface $account, UserInterface $user) {
    if (isset($user) && ($account->id() == $user->id()) && $user->hasRole('eincidencias_customer')) {
      return AccessResult::allowedIfHasPermission($account, 'access own incident');
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage permission for incident operation.
   * @param AccountInterface $account
   * @param String $operation
   * @param NodeInterface $node
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function incidentAccess(AccountInterface $account, string $operation, NodeInterface $node) {   
    switch($operation) {
      case 'access':
        if (isset($node) && ($node->getType() == 'incident'))
          return AccessResult::allowedIfHasPermission($account, 'access all incidents');
        break;
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage permission for intervention operation.
   * @param AccountInterface $account
   * @param string $operation
   * @param NodeInterface $node
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   */
  public function interventionAccess(AccountInterface $account, string $operation, NodeInterface $node) {
    switch($operation) {
      case 'add':
        if (isset($node) && ($node->getType() == 'incident')) {
          return AccessResult::allowedIfHasPermission($account, 'add intervention all incidents');
        }
        break;
      case 'access':
        if (isset($node) && ($node->getType() == 'intervention')) {
          $outsource_id = $node->get('field_eincidencias_outsource_id')->getValue()[0]['target_id'];
          $incident_id = $node->get('field_eincidencias_incident_id')->getValue()[0]['target_id'];
          $incident = \Drupal::entityTypeManager()->getStorage('node')->load($incident_id);
          $customer_id = $incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id'];
          if (in_array('eincidencias_outsource', $account->getRoles()) && ($outsource_id == $account->id())) {
            return AccessResult::allowedIfHasPermission($account, 'access own intervention');
          } else if (in_array('eincidencias_customer', $account->getRoles()) && ($customer_id == $account->id())) {
            return AccessResult::allowedIfHasPermission($account, 'access own intervention');
          }
          return AccessResult::allowedIfHasPermission($account, 'add intervention all incidents');
        }
        break;
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage permission for alter user account.
   * @param AccountProxyInterface $account
   * @param UserInterface $user
   */
  public function myProfileAccess(AccountProxyInterface $account, UserInterface $user) {
    if (isset($user)) {
      if ($account->id() == $user->id()) {
        // Check if user is altering his own data
        return AccessResult::allowedIfHasPermission($account, 'alter own user');
      } 
      return AccessResult::allowedIfHasPermission($account, 'alter all users');
    }
    
    return AccessResult::forbidden(t('Parameter Error: Null user passed'));
  }
  
  /**
   * Manage permission for message forms.
   * @param AccountProxyInterface $account
   * @param String $operation
   * @param NodeInterface $node
   */
  public function messageAccess(AccountProxyInterface $account, String $operation, NodeInterface $node = null) {
    switch($operation) {
      case 'add':
        if (isset($node) && ($node->getType() == 'intervention')) {
          $eincidencias_service = \Drupal::service('eincidencias.manager');
          $incident = $eincidencias_service->getInterventionIncident($node);
          $customer_id = $incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id'];
          $outsource_id = $node->get('field_eincidencias_outsource_id')->getValue()[0]['target_id'];
          if (!$eincidencias_service->isInterventionFinished($node)) {
            if ( $customer_id == $account->id() ) {
              return AccessResult::allowedIfHasPermission($account, 'add message own interventions');
            } else if ( $outsource_id == $account->id() ) {
              return AccessResult::allowedIfHasPermission($account, 'add message own interventions');
            } 
            return AccessResult::allowedIfHasPermission($account, 'add message all interventions');
          }
        }
        break;
    }
    
    return AccessResult::forbidden();
  }
  
  /**
   * Manage permission for counter web service access.
   */
  public function counterWSAccess(AccountProxyInterface $account) {
    $eincidencias_service = \Drupal::service('eincidencias.manager');
    $current_request = \Drupal::request();
    
    if ($current_request->query->has('technician_id')) {
      return AccessResult::allowed();
    } else if (!$eincidencias_service->emptyConfigDefaultTechnician()) {
      $technician_id = \Drupal::config('eincidencias.settings')->get('default_technician');
      $current_request->query->set('technician_id', $technician_id);
      return AccessResult::allowed();
    }
    
    return AccessResult::forbidden();
  }
	
}