<?php
namespace Drupal\eincidencias\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Session\AccountProxyInterface;

class FormController extends ControllerBase {
  protected $form_builder;
  protected $current_user;
  
  /**
   * 
   * @param FormBuilderInterface $form_builder
   */
  public function __construct(FormBuilderInterface $form_builder,
    AccountProxyInterface $current_user) {
    $this->form_builder = $form_builder;
    $this->current_user = $current_user;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Controller\FormController
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('form_builder'),
      $container->get('current_user')
    );
  }
  
  /**
   * Callback to return development titles forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function developmentTitle(string $operation, NodeInterface $node = null) {
    $title = '';
    $development = '';
    if (isset($node) ) {
      $development = $node->getTitle();
    }
    
    switch($operation) {
      case 'add':
        $title = t('Add development');
        break;
      case 'alter':
        $title = t('Alter @development', ['@development' => $development]);
        break;
      case 'access':
        $title = t('@development', ['@development' => $development]);
        break;
      case 'add-user':
        $title = t('Add customer to @development', ['@development' => $development]);
        break;
      case 'add-incident':
        $title = t('Add incident to @development', ['@development' => $development]);
        break;
      default:
        $title = t('Development');
    }
    
    return $title;
  }
  
  /**
   * Callback to return incident titles forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function incidentTitle(String $operation, NodeInterface $node = null) {
    $title = '';
    switch ($operation) {
      case 'multiply':
        $title = t('Multiply Incident');
        break;
    }
    
    return $title;
  }
  
  /**
   * Callback to return intervention titles forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function interventionTitle(string $operation, NodeInterface $node = null) {
    switch ($operation) {
      case 'add':
        $title = t('Add Intervention');
        break;
      case 'access':
        $title = t('Intervention');
        break;
    }
    
    return $title;
  }
  
  /**
   * Callback to return messages titles.
   * @param String $operation
   * @param NodeInterface $node
   * @return string
   */
  public function messageTitle(String $operation, NodeInterface $node = null) {
    $title = '';
    $title_node = isset($node) ? $node->getTitle() : '';
    
    switch($operation) {
      case 'add':
        $title = t('Add message to incident @incident', ['@incident' => $title_node]);
        break;
      case 'alter':
        $title = t('Alter message @message', ['@message' => $title_node]);
        break;
    }
    
    return $title;
  }
  
  /**
   * Show login form or redirect to dashboard if user is authenticated.
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
   */
  public function loginForm() {
    // Check if user is authenticated and send to dashboard or login
    if ($this->current_user->isAuthenticated()) {
      $return = $this->redirect('eincidencias.dashboard', ['user' => $this->current_user->id()]);
    } else {
      $return = $this->form_builder->getForm('\Drupal\eincidencias\Form\Login');
    }
    
    return $return;
  }
  
  /**
   * Manage Development forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return array
   */
  public function developmentForms(String $operation, NodeInterface $node = null) {
    switch($operation) {
      case 'add':
      case 'alter':
        $form = $this->form_builder->getForm('Drupal\eincidencias\Form\DevelopmentManageForm', $operation, $node);
        break;
      case 'access':
        $form = $this->form_builder->getForm('Drupal\eincidencias\Form\DevelopmentAccessForm', $node);
        break;
      case 'add-user':
        $form = $this->form_builder->getForm('Drupal\eincidencias\Form\CustomerForm', $node, null);
        break;
    }
    
    return $form;
  }
  
  /**
   * Manage Dashboard forms.
   * @param UserInterface $user
   * @return array
   */
  public function dashboardForms(UserInterface $user) {
    if ($user->hasRole('eincidencias_technician')) {
      $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\DashboardFormTechnician', $user);
    } else if ($user->hasRole('eincidencias_customer')) {
      $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\DashboardFormCustomer', $user);
    } else if ($user->hasRole('eincidencias_outsource')) {
      $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\DashboardFormOutsource', $user);
    } else {
      return $this->redirect('entity.user.canonical', [
        'user' => $user->id(),
      ]);
    }
    
    return $form;
  }
  
  /**
   * Manage incident add form.
   * @param string $operation
   * @param UserInterface $user
   * @return array
   */
  public function incidentAddForm(string $operation, UserInterface $user = null) {
    $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\IncidentManageForm', $user, null);
    return $form;
  }
  
  /**
   * Return Incident forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return array
   */
  public function incidentForms(string $operation, NodeInterface $node = null) {
    switch ($operation) {
      case 'access':
        $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\IncidentAccessForm', $node);
        break;
    }
    
    return $form;
  }
  
  /**
   * Return incident multiply form.
   * @param NodeInterface $node
   * @return array
   */
  public function incidentMultiplyForm(NodeInterface $node = null) {
    $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\IncidentMultiplyForm', $node);
    return $form;
  }
  
  /**
   * Return interventions forms.
   * @param string $operation
   * @param NodeInterface $node
   * @return array
   */
  public function interventionForms(string $operation, NodeInterface $node = null) {
    switch ($operation) {
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\InterventionManageForm', $node);
        break;
      case 'access':
        $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\InterventionAccessForm', $node);
        break;
    }
    
    return $form;
  }
  
  /**
   * Return User alter form.
   * @param UserInterface $user
   * @return array
   */
  public function myProfileForms(UserInterface $user = null) {
    $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\CustomerForm', null, $user);
    return $form;
  }
  
  /**
   * Return messages forms.
   * @param String $operation
   * @param NodeInterface $node
   * @return array
   */
  public function messageForms(String $operation, NodeInterface $node = null) {
    switch($operation) {
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\MessageManageForm', $node);
        break;
    }
    
    return $form;
  }
  
  public function incidentListForm(UserInterface $user) {
    $form = $this->form_builder->getForm('\Drupal\eincidencias\Form\IncidentListForm', $user);
    
    return $form;
  }
  
}