<?php
namespace Drupal\eincidencias\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use TypeError;

class WServiceController extends ControllerBase {
  protected $eincidenciasManager;
  protected $config;
  
  /**
   * 
   * @param eincidenciasManagerInterface $eincidenciasManager
   */
  public function __construct(eincidenciasManagerInterface $eincidenciasManager,
      ConfigFactoryInterface $config) {
    $this->eincidenciasManager = $eincidenciasManager;
    $this->config = $config;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Controller\WServiceController
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('eincidencias.manager'),
      $container->get('config.factory')
    );
  }
  
  /**
   * Return json with counters for a technician
   * @param int $technician
   */
  public function getCounters(Request $request) {  
    $result = [];
    $technician_id = $request->query->get('technician_id');
    
    // Get incidents not assigned for a technician
    try {
      $technician = $this->eincidenciasManager->getUser($technician_id);
      $incidents = $this->eincidenciasManager->getTechnicalIncidentNotAssigned($technician);
    } catch (TypeError $e) {
      $incidents = [];
    }
    
    $result['incidents_not_assigned'] = [
      'description' => t('Incidents not assigned'),
      'value' => count($incidents),
    ];
    
    $response = new JsonResponse($result);
    return $response;
  }
}