<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class ConfigForm extends ConfigFormBase {
  protected $current_user;
  protected $logger;
  protected $entityTypeManager;
  
  /**
   * Implement construct function.
   * @param AccountProxyInterface $current_user
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
    LoggerChannelFactoryInterface $logger,
    EntityTypeManagerInterface $entityTypeManager) {
      $this->current_user = $current_user;
      $this->logger = $logger->get('eincidencias');
      $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * Implement create function.
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'eincidenciasConfigForm';
  }
  
  /**
   * 
   * @return string[]
   */
  protected function getEditableConfigNames() {
    return [
      'eincidencias.settings'
    ];
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if incident age is int
    if (!is_numeric($form_state->getValue('incident_not_finished_age'))) {
      $form_state->setErrorByName(t('Incident Age'), t('Incident not finished age must be a numeric value'));
    }
    
    parent::validateForm($form, $form_state);
  }
  
  /**
   * Return technicians.
   * @return array
   */
  public function getTechnicians() {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties();
    $result = [];
    foreach ($users as $value) {
      if ($value->hasRole('eincidencias_technician')) {
        $result[$value->id()] = $value->get('field_eincidencias_name')->value;
      }
    }
    return $result;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eincidencias.settings');
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage eIncidencias configuration.'),
    ];
    
    $form['default_technician'] = [
      '#type' => 'select',
      '#title' => t('Default Technician'),
      '#description' => t('Select default technician.'),
      '#options' => $this->getTechnicians(),
    ];
    
    $form['config_tab'] = [
      '#type' => 'vertical_tabs',
    ];
    
    $form['notifications_enabled'] = [
      '#type' => 'details',
      '#title' => t('Notifications Enabled'),
      '#description' => t('Check enabled notifications'),
      '#group' => 'config_tab',
    ];
    
    $form['notifications_enabled']['notification_adduser'] = [
      '#type' => 'checkbox',
      '#title' => t('Add user notifications'),
      '#description' => t('Check this box to send add user notifications'),
      '#default_value' => $config->get('notification_adduser'),
    ];
    
    $form['notifications_enabled']['notification_addincident'] = [
      '#type' => 'checkbox',
      '#title' => t('Add incident notifications'),
      '#description' => t('Check this box to send add incident notifications'),
      '#default_value' => $config->get('notification_addincident'),
    ];
    
    $form['notifications_enabled']['notification_addintervention'] = [
      '#type' => 'checkbox',
      '#title' => t('Add intervention notifications'),
      '#description' => t('Check this box to send add intervention notifications'),
      '#default_value' => $config->get('notification_addintervention'),
    ];
    
    $form['notifications_enabled']['notification_addmessage'] = [
      '#type' => 'checkbox',
      '#title' => t('Add message notifications'),
      '#description' => t('Check this box to send add message notifications'),
      '#default_value' => $config->get('notification_addmessage'),
    ];
    
    $form['mails_tab'] = [
      '#type' => 'details',
      '#title' => t('Mail notifications'),
      '#description' => t('Config all mail text notifications.'),
      '#group' => 'config_tab',
    ];
    
    $form['mails_tab']['mail_adduser'] = [
      '#type' => 'textarea',
      '#title' => t('Add user mail text'),
      '#description' => t('Use this tokens: [customer:uid], [customer:name], 
        [customer:user], [customer:password]'),
      '#default_value' => $config->get('mail_adduser'),
    ];
    
    $form['mails_tab']['mail_addincident'] = [
      '#type' => 'textarea',
      '#title' => t('Add incident mail text'),
      '#description' => t('Use this tokens: [customer:uid], [customer:name], 
        [technical:uid], [technical:name], [incident:description]'),
      '#default_value' => $config->get('mail_addincident'),
    ];
    
    $form['mails_tab']['mail_addintervention'] = [
      '#type' => 'textarea',
      '#title' => t('Add intervention mail text'),
      '#description' => t('Use this tokens: [customer:uid], [customer:name], 
        [technical:uid], [technical:name], [outsource:uid], [outsource:name],
        [incident:description], [intervention:description], [intervention:enddate]'),
      '#default_value' => $config->get('mail_addintervention'),
    ];
    
    $form['mails_tab']['mail_addmessage'] = [
      '#type' => 'textarea',
      '#title' => t('Add message mail text'),
      '#description' => t('Use this tokens: [customer:uid], [customer:name],
        [technical:uid], [technical:name], [outsource:uid], [outsource:name],
        [incident:description], [intervention:description], [intervention:enddate],
        [message:author_name], [message:description]'),
      '#default_value' => $config->get('mail_addmessage'),
    ];
    
    $form['batch_tab'] = [
      '#type' => 'details',
      '#title' => t('Batch Processes'),
      '#description' => t('Config batch processes that run with cron.'),
      '#group' => 'config_tab',
    ];
    
    $form['batch_tab']['mail_incident_not_finished'] = [
      '#type' => 'textarea',
      '#title' => t('Incident not finished mail text'),
      '#description' => t('Message sendend periodically notifying incident not finished.'),
      '#default_value' => $config->get('mail_incident_not_finished'),
    ];
    
    $form['batch_tab']['incident_not_finished_age'] = [
      '#type' => 'textfield',
      '#title' => t('Incident maximum age'),
      '#description' => t('Maximum age for each incident to be included on notify mail. (In days)'),
      '#default_value' => $config->get('incident_not_finished_age'),
    ];
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('eincidencias.settings');
    $config->set('default_technician', $form_state->getValue('default_technician'));
    $config->set('mail_adduser', $form_state->getValue('mail_adduser'));
    $config->set('mail_addincident', $form_state->getValue('mail_addincident'));
    $config->set('mail_addintervention', $form_state->getValue('mail_addintervention'));
    $config->set('mail_addmessage', $form_state->getValue('mail_addmessage'));
    $config->set('mail_incident_not_finished', $form_state->getValue('mail_incident_not_finished'));
    $config->set('incident_not_finished_age', $form_state->getValue('incident_not_finished_age'));
    $config->set('notification_adduser', $form_state->getValue('notification_adduser'));
    $config->set('notification_addincident', $form_state->getValue('notification_addincident'));
    $config->set('notification_addintervention', $form_state->getValue('notification_addintervention'));
    $config->set('notification_addmessage', $form_state->getValue('notification_addmessage'));
    $config->save();
    
    // Log message
    $this->logger->info('@login - ConfigForm - Update eIncidencias settings.', [
      '@login' => $this->current_user->getAccountName(),
    ]);
    
    parent::submitForm($form, $form_state);
  }
}