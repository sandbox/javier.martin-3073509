<?php 
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Session\AccountProxyInterface;

class CustomerForm extends FormBase {
  protected $user;
  protected $development;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $email_validator;
  protected $current_user;
  /**
   *
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidencias
   */
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager, EmailValidatorInterface $email_validator,
    AccountProxyInterface $current_user) {
      $this->messenger = $messenger;
      $this->eincidenciasManager = $eincidenciasManager;
      $this->email_validator = $email_validator;
      $this->current_user = $current_user;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\CustomerForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('email.validator'),
      $container->get('current_user')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'CustomerForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (isset($this->development) && $this->eincidenciasManager->checkUserExist($form_state->getValue('mail'))) {
      $form_state->setErrorByName('username', t('User exists'));
    }
    
    if (!$this->email_validator->isValid($form_state->getValue('mail'))) {
      $form_state->setErrorByName('email', t('Invalid email'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, 
    NodeInterface $development = null, UserInterface $user = null) {
    $this->development = $development;
    $this->user = $user;
    
    $form['form_description'] = [
      '#markup' => t('Use this form to add users and link them to a development'),
    ];
    
    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#description' => t('Intro user email'),
      '#required' => TRUE,
      '#size' => 25,
      '#default_value' => isset($user) ? $user->getEmail() : '',
    ];
    
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Intro customer name.'),
      '#required' => TRUE,
      '#size' => 50,
      '#default_value' => isset($user) ? $user->get('field_eincidencias_name')->value : '',
    ];
    
    $form['address'] = [
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#description' => t('Intro customer address'),
      '#required' => TRUE,
      '#size' => 50,
      '#default_value' => isset($user) ? $user->get('field_eincidencias_address')->value : '',
    ];
    
    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#description' => t('Intro customer phone number.'),
      '#required' => TRUE,
      '#size' => 15,
      '#default_value' => isset($user) ? $user->get('field_eincidencias_phone')->value : '',
    ];
    
    $form['pass'] = [
      '#type' =>'password_confirm',
      '#size' => 10,
    ];
    
    $form['sendmail'] = [
      '#type' => 'checkbox',
      '#title' => t('Send mail notification'),
      '#description' => t('Check if you want to notify creation account to user'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (isset($this->development)) {
      $customer = $this->eincidenciasManager->addCustomer([
        'name' => $form_state->getValue('mail'),
        'mail' => $form_state->getValue('mail'),
        'pass' => $form_state->getValue('pass'),
        'field_eincidencias_name' => $form_state->getValue('name'),
        'field_eincidencias_address' => $form_state->getValue('address'),
        'field_eincidencias_phone' => $form_state->getValue('phone'),
        'field_eincidencias_develop_id' => $this->development->id(),
        'sendmail' => $form_state->getValue('sendmail'),
      ]);
      
      // Status message
      $this->messenger->addStatus(t('@user added', [
        '@user' => $customer->get('name')->value,
      ]));
      
      $form_state->setRedirect('eincidencias.development', [
        'operation' => 'access',
        'node' => $this->development->id(),
      ]);
    } else if (isset($this->user)) {
      $this->eincidenciasManager->updateCustomer($this->user, [
        'pass' => $form_state->getValue('pass'),
        'field_eincidencias_name' => $form_state->getValue('name'),
        'field_eincidencias_address' => $form_state->getValue('address'),
        'field_eincidencias_phone' => $form_state->getValue('phone'),
      ]);
      
      // Status message
      $this->messenger->addStatus(t('@user updated', [
        '@user' => $this->user->getAccountName(),
      ]));
      
      if ($this->current_user->id() == $this->user->id()) {
        $form_state->setRedirect('eincidencias.dashboard', [
          'user' => $this->user->id(),
        ]);
      }
    }
  }
}