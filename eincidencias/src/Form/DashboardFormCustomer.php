<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;

class DashboardFormCustomer extends FormBase {
  protected $user;
  
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  /**
   *
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidencias
   */
  public function __construct(eincidenciasManagerInterface $eincidenciasManager,
    eincidenciasFormatterInterface $eincidenciasFormatter) {
      $this->eincidenciasManager = $eincidenciasManager;
      $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DashboardCustomerForm';
  }
  
  /**
   * Return interventions assigned table
   * @return array
   */  
  protected function getInterventionsAssignedTable() {
    $interventions = $this->eincidenciasManager->getCustomerInterventionsNotEnded($this->user);
    
    return $this->eincidenciasFormatter->getInterventionsNotEnded($interventions);
  }
  
  /**
   * Return interventions ended table
   * @return array
   */
  protected function getInterventionsEndedTable() {
    $interventions = $this->eincidenciasManager->getCustomerInterventionsEnded($this->user);
    
    return $this->eincidenciasFormatter->getInterventionsEnded($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    $form['form_description'] = [
      '#markup' => t('Use this form to access to your interventions.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['interventions_assigned_title'] = [
      '#markup' => t('Interventions assigned'),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['interventions_assigned_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'date_creation' => t('Creation date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getInterventionsAssignedTable() as $key => $value) {
      $form['interventions_assigned_table'][$key] = $value;
    }
    
    $form['interventions_ended_title'] = [
      '#markup' => t('Interventions ended'),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['interventions_ended_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'date_end' => t('End date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getInterventionsEndedTable() as $key => $value) {
      $form['interventions_ended_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state, UserInterface $user = null) {
    
  }
}