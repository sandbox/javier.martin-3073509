<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\user\UserInterface;

class DashboardFormOutsource extends FormBase {
  protected $user;
  
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  /**
   *
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidencias
   */
  public function __construct(eincidenciasManagerInterface $eincidenciasManager,
      eincidenciasFormatterInterface $eincidenciasFormatter) {
        $this->eincidenciasManager = $eincidenciasManager;
        $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('eincidencias.manager'),
        $container->get('eincidencias.formatter')
        );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DashboardOutsourceForm';
  }
  
  /**
   * Return interventions not ended table
   * @return array
   */
  protected function getInterventionsNotEnded() {
    $interventions = $this->eincidenciasManager->getOutsourceInterventionsNotEnded($this->user);
    
    return $this->eincidenciasFormatter->getInterventionsNotEnded($interventions);
  }
  
  /**
   * Return interventions ended table
   * @return table
   */
  protected function getInterventionsEnded() {
    $interventions = $this->eincidenciasManager->getOutsourceInterventionsEnded($this->user);
    
    return $this->eincidenciasFormatter->getInterventionsEnded($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage development, your incident and incident not assigned to any technical.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['interventions_not_ended_title'] = [
      '#markup' => t('Interventions not ended'),
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['interventions_not_ended_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'date_creation' => t('Creation date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getInterventionsNotEnded() as $key => $value) {
      $form['interventions_not_ended_table'][$key] = $value;
    }
    
    $form['interventions_ended_title'] = [
      '#markup' => t('Interventions ended'),
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['interventions_ended_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'date_creation' => t('Ended date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getInterventionsEnded() as $key => $value) {
      $form['interventions_ended_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}