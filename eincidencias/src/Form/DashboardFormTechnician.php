<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\user\UserInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;

class DashboardFormTechnician extends FormBase {
  protected $user;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  /**
   * 
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidenciasManager
   * @param eincidenciasFormatterInterface $eincidenciasFormatter
   */
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager,
    eincidenciasFormatterInterface $eincidenciasFormatter) {
        $this->messenger = $messenger;
        $this->eincidenciasManager = $eincidenciasManager;
        $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DashboardTechnicianForm';
  }
  
  /**
   * Return incidents not assigned table
   * @return array
   */
  protected function getTechnicalIncidentsNotAssigned() {
    $incidents = $this->eincidenciasManager->getTechnicalIncidentNotAssigned($this->user);
    return $this->eincidenciasFormatter->getIncidentsNotAssignedTable($incidents);
  }
  
  /**
   * Return interventions not ended table
   * @return array
   */
  protected function getTechnicalInterventionsNotEnded() {
    $interventions = $this->eincidenciasManager->getTechnicalInterventionsNotEnded($this->user);
    return $this->eincidenciasFormatter->getInterventionsNotEnded($interventions);
  }
  
  /**
   * Return developments table
   * @return array
   */
  protected function getDevelopmentsTable() {
    $developments = $this->eincidenciasManager->getDevelopments();
    return $this->eincidenciasFormatter->getDevelopmentsTable($developments);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    $form['incidents_not_assigned_title'] = [
      '#markup' => t('Incidents not assigned'),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['incidents_not_assigned_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'development' => t('Development'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'date-creation' => t('Creation Date'),
        'access' => t('Access'),
      ],
      '#empty' => t('No incidents found.'),
    ];
    
    foreach ($this->getTechnicalIncidentsNotAssigned() as $key => $value) {
      $form['incidents_not_assigned_table'][$key] = $value;
    }
    
    $form['dashboard'] = [
      '#type' => 'vertical-tabs',
    ];
    
    $form['interventions_assigned'] = [
      '#type' => 'details',
      '#title' => t('Interventions assigned'),
      '#description' => 'Access to your interventions assigned.',
      '#group' => 'dashboard',
    ];
    
    $form['interventions_assigned']['table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'creation_date' => t('Creation Date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getTechnicalInterventionsNotEnded() as $key => $value) {
      $form['interventions_assigned']['table'][$key] = $value;
    }
    
    $form['developments'] = [
      '#type' => 'details',
      '#title' => t('Development'),
      '#description' => 'Access to your development.',
      '#group' => 'dashboard',
    ];
    
    $form['developments']['table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'access' => t('Access'),
      ],
      '#empty' => t('No developments found.'),
    ];
    
    foreach ($this->getDevelopmentsTable() as $key => $value) {
      $form['developments']['table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}