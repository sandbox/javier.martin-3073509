<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;

class DevelopmentAccessForm extends FormBase {
  protected $node;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  /**
   * 
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidenciasManager
   * @param eincidenciasFormatterInterface $eincidenciasFormatter
   */
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager,
    eincidenciasFormatterInterface $eincidenciasFormatter) {
    $this->messenger = $messenger;
    $this->eincidenciasManager = $eincidenciasManager;
    $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentAccessForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DevelopmentAccessForm';
  }
  
  /**
   * Return customers table
   * @return array
   */
  protected function getCustomerTable() {
    $customers = $this->eincidenciasManager->getDevelopmentCustomers($this->node);
    return $this->eincidenciasFormatter->getCustomersTable($customers);
  }
  
  /**
   * Return incidents not assigned table
   * @return array
   */
  protected function getDevelopmentsIncidentsNotAssignedTable() {
    $incidents = $this->eincidenciasManager->getDevelopmentIncidentsNotAssigned($this->node);
    return $this->eincidenciasFormatter->getIncidentsNotAssignedTable($incidents);
  }
  
  /**
   * Return interventions not ended
   * @return array
   */
  protected function getDevelopmentInterventionsNotEndedTable() {
    $interventions = $this->eincidenciasManager->getDevelopmentInterventionsNotEnded($this->node);
    return $this->eincidenciasFormatter->getInterventionsNotEnded($interventions);
  }
  
  /**
   * Return interventions ended
   * @return array
   */
  protected function getDevelopmentInterventionsEndedTable() {
    $interventions = $this->eincidenciasManager->getDevelopmentInterventionsEnded($this->node);
    return $this->eincidenciasFormatter->getInterventionsEnded($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = null) {
    $this->node = $node;
    
    $form['form_description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Use this form to manage developments.'),
    ];
    
    $form['development_description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->node->get('body')->value,
    ];
    
    $form['customer_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => t('Customers'),
    ];
    
    $form['customer_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'name' => t('Name'),
        'mail' => t('Mail'),
        'address' => t('Address'),
        'alter_user' => t('Alter User'),
        'access' => t('Dashboard')
      ],
      '#empty' => t('No customers found.'),
    ];
    
    foreach($this->getCustomerTable() as $key => $value){
      $form['customer_table'][$key] = $value;
    }
    
    $form['incidents_not_assigned_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => t('Incidents not assigned'),
    ];
    
    $form['incident_not_assigned'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'development' => t('Development'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'date-creation' => t('Creation Date'),
        'access' => t('Access'),
      ],
      '#empty' => t('No incidents found.'),
    ];
    
    foreach ($this->getDevelopmentsIncidentsNotAssignedTable() as $key => $value) {
      $form['incident_not_assigned'][$key] = $value;
    }
    
    $form['intervention_not_ended_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => t('Interventions not ended'),
    ];
    
    $form['intervention_not_ended'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'creation_date' => t('Creation Date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getDevelopmentInterventionsNotEndedTable() as $value) {
      $form['intervention_not_ended'][] = $value;
    }
    
    $form['intervention_ended_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => t('Interventions ended'),
    ];
    
    $form['intervention_ended'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'date_end' => t('End Date'),
        'customer' => t('Customer'),
        'address' => t('Address'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getDevelopmentInterventionsEndedTable() as $value) {
      $form['intervention_ended'][] = $value;
    }
    
    $form_state->disableCache();
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}