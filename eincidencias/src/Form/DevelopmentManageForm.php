<?php 
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

class DevelopmentManageForm extends FormBase {
  protected $operation;
  protected $node;
  
  protected $messenger;
  protected $eincidencias;
  
  /**
   * 
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidencias
   */
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidencias) {
    $this->messenger = $messenger;
    $this->eincidencias = $eincidencias;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DevelopmentForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, String $operation = null, NodeInterface $node = null) {
    $this->operation = $operation;
    $this->node = $node;
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage development.'),
    ];
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Intro development name'),
      '#default_value' => isset($node) ? $node->getTitle() : '',
      '#required' => TRUE,
    ];
    
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#description' => t('Intro development description'),
      '#default_value' => isset($node) ? $node->get('body')->value : '',
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch($this->operation) {
      case 'add':
        $development = $this->eincidencias->addDevelopment([
          'title' => $form_state->getValue('title'),
          'body' => $form_state->getValue('body'),
        ]);
        
        // Status message
        $this->messenger->addStatus(t('@development added', [
          '@development' => $development->getTitle(),
        ]));
        
        break;
      case 'alter':
        $this->eincidencias->updateDevelopment($this->node, [
          'title' => $form_state->getValue('title'),
          'body' => $form_state->getValue('body'),
        ]);

        // Status message
        $this->messenger->addStatus(t('@development updated', [
          '@development' => $this->node->getTitle(),
        ]));
        
        break;
    }
    
    $form_state->setRedirect('eincidencias.development', [
      'operation' => 'access',
      'node' => (isset($this->node)) ? $this->node->id() : $development->id(),
    ]);
  }
}