<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;

class IncidentAccessForm extends FormBase {
  protected $node;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager,
    eincidenciasFormatterInterface $eincidenciasFormatter) {
      $this->messenger = $messenger;
      $this->eincidenciasManager = $eincidenciasManager;
      $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'IncidentAccessForm';
  }
  
  /**
   * Return interventions table for a incident
   * @return array
   */
  protected function getIncidentInterventionsTable() {
    $interventions = $this->eincidenciasManager->getIncidentInterventions($this->node);
    return $this->eincidenciasFormatter->getInterventionsIncidentsTable($interventions);
  }
  
  /**
   * Return files table for a incident
   * @return array
   */
  protected function getIncidentFilesTable() {
    $files = $this->eincidenciasManager->getFilesEntity($this->node);
    return $this->eincidenciasFormatter->getFilesTable($files);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = null) {
    $this->node = $node;
    $customer = $this->eincidenciasManager->getUser($node->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
    $technical = $this->eincidenciasManager->getUser($node->get('field_eincidencias_technical_id')->getValue()[0]['target_id']);
    $development = $this->eincidenciasManager->getNodeEntity($customer->get('field_eincidencias_develop_id')->getValue()[0]['target_id']);
    
    $form['form_description'] = [
      '#markup' => t('Use this form to generate interventions from a incident.'),
    ];
    
    $form['development'] = [
      '#markup' => $development->getTitle(),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['technical'] = [
      '#markup' => $technical->get('field_eincidencias_name')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3></br>',
    ];
    
    $form['customer'] = [
      '#markup' => $customer->get('field_eincidencias_name')->value,
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['customer_address'] = [
      '#markup' => $customer->get('field_eincidencias_address')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['customer_phone'] = [
      '#markup' => $customer->get('field_eincidencias_phone')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3></br>',
    ];
    
    $form['description'] = [
      '#markup' => $node->get('body')->value,
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['files'] = [
      '#type' => 'table',
      '#header' => [
        'file' => t('Files'),
      ],
      '#empty' => t('No files found.'),
    ];
    
    foreach ($this->getIncidentFilesTable() as $key => $value) {
      $form['files'][$key] = $value;
    }
    
    $form['interventions_title'] = [
      '#markup' => t('Interventions'),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['interventions_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => t('Id'),
        'description' => t('Description'),
        'outsource' => t('Outsource'),
        'creation_date' => t('Creation Date'),
        'access' => t('Access'),
      ],
      '#empty' => t('No interventions found.'),
    ];
    
    foreach ($this->getIncidentInterventionsTable() as $key => $value) {
      $form['interventions_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}
}