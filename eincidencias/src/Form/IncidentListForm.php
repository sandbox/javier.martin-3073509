<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\eincidencias\eincidenciasFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\node\NodeInterface;

class IncidentListForm extends FormBase {
  protected $customer;
  
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  public function __construct(eincidenciasManagerInterface $eincidenciasManager,
      eincidenciasFormatterInterface $eincidenciasFormatter) {
    $this->eincidenciasManager = $eincidenciasManager;
    $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  public function getFormId() {
    return 'IncidentListForm';
  }
  
  protected function getIncidentInterventionTable(NodeInterface $incident) {
    $interventions = $this->eincidenciasManager->getIncidentInterventions($incident);
    return $this->eincidenciasFormatter->getInterventionsNotEnded($interventions);
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $customer = null) {
    $this->customer = $customer;
    
    $form['incidents'] = [
      '#type' => 'vertical-tabs',
    ];
    
    $incidents = $this->eincidenciasManager->getCustomerIncidents($customer);
    foreach ($incidents as $key => $value) {
      $form[$key] = [
        '#type' => 'details',
        '#title' => $value->get('body')->value,
        '#description' => t('Interventions'),
        '#group' => 'incidents',
      ];
      
      $form[$key]['interventions'] = [
        '#type' => 'table',
        '#header' => [
          'id' => t('Id'),
          'development' => t('Development'),
          'customer' => t('Customer'),
          'address' => t('Address'),
          'date-creation' => t('Creation Date'),
          'access' => t('Access'),
        ],
        '#empty' => t('No incidents found.'),
      ];
      
      foreach ($this->getIncidentInterventionTable($value) as $key_intervention => $value_intervention) {
        $form[$key]['interventions'][$key_intervention] = $value_intervention;
      }
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}