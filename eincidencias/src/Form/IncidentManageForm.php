<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\node\NodeInterface;

class IncidentManageForm extends FormBase {
  protected $user;
  protected $node;
  
  protected $messenger;
  protected $eincidenciasManager;
  
  public function __construct(MessengerInterface $messenger, 
    eincidenciasManagerInterface $eincidenciasManager) {
      $this->messenger = $messenger;
      $this->eincidenciasManager = $eincidenciasManager;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'IncidentManagementForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->eincidenciasManager->emptyConfigDefaultTechnician()) {
      $form_state->setErrorByName('Config Technician', t('Default technician not config'));
    }
    if (!$this->user->isActive()) {
      $form_state->setErrorByName('user', t('User is not active'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null, NodeInterface $node = null) {
    $this->user = $user;
    $this->node = $node;
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage incidents.'),
    ];
    
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#description' => t('Intro incident description.'),
      '#required' => TRUE,
      '#default_value' => isset($node) ? $node->get('body')->value : '',
    ];
    
    $form['files'] = [
      '#type' => 'managed_file',
      '#multiple' => TRUE,
      '#title' => t('Files'),
      '#upload_location' => 'private://eincidencias/user/' . $user->id(),
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg'],
      ],
      '#description' => $this->t('Upload files. Allowed extensions: .jpg'),
      '#default_values' => isset($node) ? $this->eincidencias->getFiles($node) : [],
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (isset($this->user)) {
      $incident = $this->eincidenciasManager->addIncident($this->user, [
        'body' => $form_state->getValue('body'),
        'files' => $form_state->getValue('files'),
      ]);
      
      // Status message
      if (isset($incident)) {
        $this->messenger->addStatus(t('Added new incident'));
      } else {
        $this->messenger->addError(t('Error adding incident'));
      }
    } else {
      $this->eincidenciasManager->updateIncident($this->node, [
        'body' => $form_state->getValue('body'),
        'files' => $form_state->getValue('files'),
      ]);
      
      // Status message
      $this->messenger->addStatus(t('Updated incident'));
    }
    
    $form_state->setRedirect('eincidencias.dashboard', [
      'user' => $this->user->id(),
    ]);
  }
}