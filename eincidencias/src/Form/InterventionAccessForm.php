<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\eincidencias\eincidenciasFormatterInterface;

class InterventionAccessForm extends FormBase {
  protected $node;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $eincidenciasFormatter;
  
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager,
    eincidenciasFormatterInterface $eincidenciasFormatter) {
        $this->messenger = $messenger;
        $this->eincidenciasManager = $eincidenciasManager;
        $this->eincidenciasFormatter = $eincidenciasFormatter;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('eincidencias.formatter')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'InterventionAccessForm';
  }
  
  protected function getInterventionMessagesTable() {
    $messages = $this->eincidenciasManager->getInterventionMessages($this->node);
    $table = [];
    foreach ($messages as $key => $value) {
      $author = $value->getOwner();
      $files = $this->eincidenciasManager->getFilesEntity($value);
      $table[$key] = [
        'messages' => [
          'author' => [
            '#markup' => isset($author->get('field_eincidencias_name')->value) ? 
            t('From') . ': ' . $author->get('field_eincidencias_name')->value : t('From') . ': ' . $author->getAccountName(),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          ],
          'message' => [
            '#markup' => $value->get('body')->value,
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          ],
          'files' => [
            '#type' => 'table',
            '#header' => [
              'files' => t('Files'),
            ],
            '#empty' => t('No files found.'),
          ],
        ],
      ];
      foreach ($files as $file) {
        $file_uri = $file->getFileUri();
        $url = Url::fromUri(file_create_url($file_uri));
        $link = Link::fromTextAndUrl($file->getFilename(), $url);
        $table[$key]['messages']['files'][$file->id()] = [
          'files' =>  $link->toRenderable(),
        ];
      }
    }
    
    return $table;
  }
  
  /**
   * Return table with incident files table.
   * @return array
   */
  protected function getIncidentFilesTable() {
    $incident = $this->eincidenciasManager->getNodeEntity($this->node->get('field_eincidencias_incident_id')->getValue()[0]['target_id']);
    $files = $this->eincidenciasManager->getFilesEntity($incident);
    return $this->eincidenciasFormatter->getFilesTable($files);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = null) {
    $this->node = $node;
    $outsource = $this->eincidenciasManager->getUser($node->get('field_eincidencias_outsource_id')->getValue()[0]['target_id']);
    $incident = $this->eincidenciasManager->getNodeEntity($node->get('field_eincidencias_incident_id')->getValue()[0]['target_id']);
    $customer = $this->eincidenciasManager->getUser($incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
    $development = $this->eincidenciasManager->getNodeEntity($customer->get('field_eincidencias_develop_id')->getValue()[0]['target_id']);
    
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage interventions'),
    ];
    
    $form['development_title'] = [
      '#markup' => $development->getTitle(),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    
    $form['address'] = [
      '#markup' => $customer->get('field_eincidencias_address')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['name'] = [
      '#markup' => $customer->get('field_eincidencias_name')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['phone'] = [
      '#markup' => $customer->get('field_eincidencias_phone')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['incident_description'] = [
      '#markup' => $incident->get('body')->value,
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['incident_files'] = [
      '#type' => 'table',
      '#header' => [
        'files' => t('Files'),
      ],
      '#empty' => t('No files found.'),
    ];
    
    foreach ($this->getIncidentFilesTable() as $key => $value) {
      $form['incident_files'][$key] = $value;
    }
    
    $form['intervention_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Intervention'),
    ];
    
    $form['intervention_fieldset']['outsource'] = [
      '#markup' => $outsource->get('field_eincidencias_name')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['intervention_fieldset']['description'] = [
      '#markup' => $node->getTitle(),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['message_table'] = [
      '#type' => 'table',
      '#header' => [
        'messages' => t('Messages'),
      ],
      '#empty' => t('No messages found.'),
    ];
    
    foreach ($this->getInterventionMessagesTable() as $key => $value) {
      $form['message_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}