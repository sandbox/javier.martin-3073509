<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class InterventionManageForm extends FormBase {
  protected $node;
  protected $incident;
  protected $customer;
  
  protected $messenger;
  protected $eincidenciasManager;
  
  /**
   * 
   * @param MessengerInterface $messenger
   * @param eincidenciasManagerInterface $eincidencias
   */
  public function __construct(MessengerInterface $messenger,
      eincidenciasManagerInterface $eincidenciasManager) {
        $this->messenger = $messenger;
        $this->eincidenciasManager = $eincidenciasManager;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('messenger'),
        $container->get('eincidencias.manager')
        );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'IncidentManagementForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = $this->eincidenciasManager->getUser($form_state->getValue('outsource'));
    if (!$user->hasRole('eincidencias_outsource')) {
      $form_state->setErrorByName('outsource', t('Outsource select error'));
    }
    if (!$user->isActive()) {
      $form_state->setErrorByName('outsource', t('Outsource is not active'));
    }
    if (!$this->node->isPublished()) {
      $form_state->setErrorByName('node', 'Node not published');
    }
  }
  
  /**
   * Return all outsources.
   * @return array
   */
  public function getOutsources() {
    $outsources = $this->eincidenciasManager->getUsers('eincidencias_outsource');
    $table = [];
    foreach ($outsources as $key => $value) {
      $table[$key] = $value->get('field_eincidencias_name')->value;
    }
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = null) {
    $this->node = $node;
    
    if ($node->getType() == 'incident') {
      $this->incident = $node;
    } else {
      $incident_id = $node->get('field_eincidencias_incident_id')->getValue()[0]['target_id'];
      $incident = $this->eincidenciasManager->getNodeEntity($incident_id);
    }
    $customer_id = $this->incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id'];
    $this->customer = $this->eincidenciasManager->getUser($customer_id);
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage incidents.'),
    ];
    
    $form['incident_data'] = [
      '#type' => 'fieldset',
      '#title' => t('Incident data'),
    ];
    
    $form['incident_data']['name'] = [
      '#markup' => $this->customer->get('field_eincidencias_name')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['incident_data']['phone'] = [
      '#markup' => $this->customer->get('field_eincidencias_phone')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['incident_data']['address'] = [
      '#markup' => $this->customer->get('field_eincidencias_address')->value,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    
    $form['incident_data']['incident_description'] = [
      '#markup' => $this->incident->get('body')->value,
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    
    $form['outsource'] = [
      '#type' => 'select',
      '#title' => t('Select Outsource'),
      '#description' => t('Select outsource.'),
      '#options' => $this->getOutsources(),
      '#required' => TRUE,
      '#default_value' => ($node->getType() == 'intervention') ?
        $node->get('field_eincidencias_outsource_id')->getValue()[0]['target_id'] : null,
    ];
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#description' => t('Intro intervention description.'),
      '#required' => TRUE,
      '#default_value' => ($node->getType() == 'intervention') ?
        $node->getTitle() : '',
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->node->getType() == 'incident') {
      $intervention = $this->eincidenciasManager->addIntervention([
        'title' => $form_state->getValue('title'),
        'field_eincidencias_outsource_id' => $form_state->getValue('outsource'),
        'field_eincidencias_incident_id' => $this->node->id(),
        'field_eincidencias_date_end' => '',
      ]);
      
      if (isset($intervention)) {
        $this->messenger->addMessage(t('Added intervention'));
      } else {
        $this->messenger->addError(t('Error adding intervention'));
      }
    } else {
      $this->eincidenciasManager->updateIntervention($this->node, [
        'title' => $form_state->getValue('title'),
      ]);
      $this->messenger->addMessage(t('Added intervention'));
    }
    $form_state->setRedirect('eincidencias.incident', [
      'operation' => 'access',
      'node' => $this->node->id(),
    ]);
  }
}