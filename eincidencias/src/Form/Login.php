<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class Login extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $messenger;
  protected $passwordHasher;
  
  /**
   * 
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   * @param MessengerInterface $messenger
   * @param PasswordInterface $passwordHasher
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager,
      LoggerChannelFactoryInterface $logger,
      MessengerInterface $messenger,
      PasswordInterface $passwordHasher) {
        $this->current_user = $current_user;
        $this->entityTypeManager = $entityTypeManager;
        $this->logger = $logger->get('eincidencias');
        $this->messenger = $messenger;
        $this->passwordHasher = $passwordHasher;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('password')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'LoginForm';
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $login = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    
    $storage = $this->entityTypeManager->getStorage('user');
    $users = $storage->loadByProperties(['name' => $login]);
    $user = reset($users);
    
    if (!$this->current_user->isAnonymous()) {
      $form_state->setErrorByName('user', t('You are already logged. Logout and back to this url to do login again.'));
    }
    
    if (empty($user)) {
      $form_state->setErrorByName('user', t('@user not exists', ['@user' => $login]));
    } else {
      if (!$this->passwordHasher->check($password, $user->get('pass')->value)) {
        $form_state->setErrorByName('password', t('Password incorrect'));
      }
      
      if ( 
        (!$user->hasRole('eincidencias_technician')) && 
        (!$user->hasRole('eincidencias_outsource')) &&
        (!$user->hasRole('eincidencias_customer'))
      ) {
        $form_state->setErrorByName('permission', t('User not allowed to login.'));
      }
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#description' => t('Intro your username'),
    ];
    
    $form['password'] = [
      '#type' => 'password',
      '#title' => t('Password'),
      '#description' => t('Intro your password'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $login = $form_state->getValue('username');
    
    $storage = $this->entityTypeManager->getStorage('user');
    $users = $storage->loadByProperties(['name' => $login]);
    $user = reset($users);
    
    user_login_finalize($user);
    
    $this->logger->info('@login - Login - Login successfull for @user.', [
      '@login' => $this->current_user->getUsername(),
      '@user' => $user->getUsername(),
    ]);
    
    $form_state->setRedirect('eincidencias.dashboard', ['user' => $user->id()]);
  }
}