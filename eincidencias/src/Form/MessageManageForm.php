<?php
namespace Drupal\eincidencias\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\eincidencias\eincidenciasManagerInterface;
use Drupal\Core\Session\AccountProxy;

class MessageManageForm extends FormBase {
  protected $user;
  protected $node;
  
  protected $messenger;
  protected $eincidenciasManager;
  protected $current_user;
  
  public function __construct(MessengerInterface $messenger,
    eincidenciasManagerInterface $eincidenciasManager, AccountProxy $current_user) {
      $this->messenger = $messenger;
      $this->eincidenciasManager = $eincidenciasManager;
      $this->current_user = $current_user;
  }
  
  /**
   *
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\Form\DevelopmentManageForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('messenger'),
      $container->get('eincidencias.manager'),
      $container->get('current_user')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'MessageManagementForm';
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $current_user = $this->eincidenciasManager->getUser($this->current_user->id());
    
    if (!$this->node->isPublished()) {
      $form_state->setErrorByName('node', t('Node not published'));
    }
    if (($this->node->getType() == 'intervention') && 
        ($this->eincidenciasManager->isInterventionFinished($this->node))) {
      $form_state->setErrorByName('intervention finished', t('Intervention finished'));
    }
    if ($current_user->hasRole('eincidencias_outsource') && 
        $current_user->hasRole('eincidencias_technician') && 
        (null !== $form_state->getValue('finish_incident')) && 
        $form_state->getValue('finish_incident')) {
      $form_state->setErrorByName('finish intervention', t('You are not allowed to finish intervention'));      
    }
    if ($current_user->hasRole('eincidencias_outsource') && 
        (null !== $form_state->getValue('finish_incident')) && 
        $form_state->getValue('finish_incident') && 
        empty($form_state->getValue('files')) ) {
      $form_state->setErrorByName('finish intervention', t('No finish document added'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = null) {
    $this->node = $node;
    if ($node->getType() == 'intervention') {
      $outsource_id = $node->get('field_eincidencias_outsource_id')->getValue()[0]['target_id'];
      $intervention_id = $node->id();
    } else {
      $outsource_id = 0;
    }
    if ($node->getType() == 'message') {
      $intervention = $this->eincidenciasManager->getMessageIntervention($node);
      $intervention_ended = ($intervention->get('field_eincidencias_date_end')->getValue() == 0) ?
        FALSE : TRUE;
      $intervention_id = $intervention->id();
    } else {
      $intervention_ended = FALSE;
    }
    
    $form['form_description'] = [
      '#markup' => t('Use this form to manage a incident message.'),
    ];
    
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#description' => t('Intro your message.'),
      '#default_value' => ($node->getType() == 'message') ? $node->get('body')->value : '',
    ];
    
    if (
        ($this->current_user->hasPermission('finish all interventions')) ||
        ($this->current_user->hasPermission('finish own intervention') && ($this->current_user->id() == $outsource_id))
    ) {
      $form['finish_incident'] = [
        '#type' => 'checkbox',
        '#title' => t('Finish incident'),
        '#description' => t('Check to finish incident.'),
        '#default_value' => $intervention_ended,
      ];
    }
    
    $form['files'] = [
      '#type' => 'managed_file',
      '#multiple' => TRUE,
      '#title' => t('Files'),
      '#upload_location' => 'private://eincidencias/intervention/' . $intervention_id,
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg'],
      ],
      '#description' => $this->t('Upload files. Allowed extensions: .jpg'),
      '#default_value' => ($node->getType() == 'message') ? $this->eincidenciasManager->getFiles($node) : [],
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    
    $form_state->disableCache();
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->node->getType() == 'intervention') {
      $message = $this->eincidenciasManager->addMessage([
        'title' => 'Message from ' . $this->current_user->getAccountName(),
        'body' => $form_state->getValue('body'),
        'files' => $form_state->getValue('files'),
        'field_eincidencias_interven_id' => $this->node->id(),
      ]);
      
      if ($form_state->getValue('finish_incident')) {
        $this->eincidenciasManager->updateIntervention($this->node, [
          'field_eincidencias_date_end' => time(),
        ]);
        
        // Status message
        $this->messenger->addStatus(t('Finish intervention'));
      }
      
      // Status message
      if (isset($message)) {
        $this->messenger->addStatus(t('Added new message'));
      } else {
        $this->messenger->addError(t('Error adding message'));
      }
    } else {
      $this->eincidenciasManager->updateMessage($this->node, [
        'body' => $form_state->getValue('body'),
        'files' => $form_state->getValue('files'),
      ]);
      
      //Status message
      $this->messenger->addStatus(t('Updated message'));
    }
    
    if ($this->node->getType() == 'intervention') {
      $form_state->setRedirect('eincidencias.intervention', [
        'operation' => 'access',
        'node' => $this->node->id(),
      ]);
    }
  }
}