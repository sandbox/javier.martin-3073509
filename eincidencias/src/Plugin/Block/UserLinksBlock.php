<?php
namespace Drupal\eincidencias\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * 
 * Provides a block that shows field_eincidencias_name for each user.
 * 
 * @Block (
 *  id = "eincidencias_userlinks_block",
 *  admin_label = @Translation("Eincidencias - User Links Block")
 * )
 */
class UserLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $current_user;
  protected $entityTypeManager;
  
  /**
   * 
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, 
    AccountProxyInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return \Drupal\eincidencias\Plugin\Block\UserNameBlock
   */
  public static function create(ContainerInterface $container, array $configuration, 
    $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }
  
  public function build() {
    $storage_user = $this->entityTypeManager->getStorage('user');
    $user = $storage_user->load($this->current_user->id());
    $form = [];
    
    if ($user->hasRole('eincidencias_technician') || 
      $user->hasRole('eincidencias_customer') ||
      $user->hasRole('eincidencias_outsource')) {
      $url_dashboard = Url::fromRoute('eincidencias.dashboard', [
        'user' => $user->id(),
      ]);
      $link_dashboard = Link::fromTextAndUrl(t('Dashboard'), $url_dashboard);
      
      $url_myprofile = Url::fromRoute('eincidencias.myprofile', [
        'user' => $user->id(),
      ]);
      $link_myprofile = Link::fromTextAndUrl(t('My profile'), $url_myprofile);
      
      $url_logout = Url::fromRoute('user.logout');
      $link_logout = Link::fromTextAndUrl(t('Logout'), $url_logout);
      
      $form['actions'] = [
        '#type' => 'actions',
      ];
      
      $form['actions']['dashboard'] = $link_dashboard->toRenderable();
      $form['actions']['space'] = [
        '#markup' => ' | ',
      ];
      $form['actions']['myprofile'] = $link_myprofile->toRenderable();
      $form['actions']['space2'] = [
        '#markup' => ' | ',
      ];
      $form['actions']['logout'] = $link_logout->toRenderable();
    }
    
    return $form;
  }
}