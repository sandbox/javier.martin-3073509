<?php
namespace Drupal\eincidencias\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * 
 * Provides a block that shows field_eincidencias_name for each user.
 * 
 * @Block (
 *  id = "eincidencias_username_block",
 *  admin_label = @Translation("Eincidencias - UserName Block")
 * )
 */
class UserNameBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $current_user;
  protected $entityTypeManager;
  
  /**
   * 
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, 
    AccountProxyInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * 
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return \Drupal\eincidencias\Plugin\Block\UserNameBlock
   */
  public static function create(ContainerInterface $container, array $configuration, 
    $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Block\BlockPluginInterface::build()
   */
  public function build() {
    $storage_user = $this->entityTypeManager->getStorage('user');
    $user = $storage_user->load($this->current_user->id());
    $form = [];
    
    $username = !empty($user->get('field_eincidencias_name')->value) ? 
    $user->get('field_eincidencias_name')->value : $user->getAccountName();
    $form['username'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $username,
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Plugin\ContextAwarePluginBase::getCacheMaxAge()
   */
  public function getCacheMaxAge() {
    return 0;
  }
}