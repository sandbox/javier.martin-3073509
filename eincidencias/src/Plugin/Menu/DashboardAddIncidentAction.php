<?php 
namespace Drupal\eincidencias\Plugin\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class DashboardAddIncidentAction extends LocalActionDefault {
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $user = $route_match->getParameter('user');
    
    return [
      'user' => $user->id(),
    ];
  }
}