<?php 
namespace Drupal\eincidencias\Plugin\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class IncidentAddMessageAction extends LocalActionDefault {
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $node = $route_match->getParameter('node');
    
    return [
      'operation' => 'add',
      'node' => $node->id(),
    ];
  }
}