<?php 
namespace Drupal\eincidencias\Plugin\Menu;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Menu\MenuLinkBase;


class MyIncidentsLinkMenu extends MenuLinkBase {
  
  public function getRouteParameters() {
    $user = \Drupal::routeMatch()->getParameter('user');
    
    return [
      'user' => isset($user) ? $user->id() : 1,
    ];
  }
  
  public function getTitle() {
    return t('My incidents');
  }
  
  public function getDescription() {
    return t('Access to your incident history.');
  }
  
  public function updateLink(array $new_definition_values, $persist) {
    throw new PluginException('Inaccessible menu link plugins do not support updating');
  }
  
  public function getCacheMaxAge() {
    return 0;
  }
}