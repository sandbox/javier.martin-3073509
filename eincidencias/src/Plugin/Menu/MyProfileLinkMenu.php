<?php 
namespace Drupal\eincidencias\Plugin\Menu;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Menu\MenuLinkBase;

class MyProfileLinkMenu extends MenuLinkBase {
  
  public function getRouteParameters() {    
    return [
      'user' => \Drupal::service('current_user')->id(),
    ];
  }
  
  public function getTitle() {
    return t('My profile');
  }
  
  public function getDescription() {
    return t('Access to your profile.');
  }
  
  public function updateLink(array $new_definition_values, $persist) {
    throw new PluginException('Inaccessible menu link plugins do not support updating');
  }
  
  public function getCacheMaxAge() {
    return 0;
  }
}