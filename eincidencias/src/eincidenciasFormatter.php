<?php
namespace Drupal\eincidencias;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

class eincidenciasFormatter implements eincidenciasFormatterInterface {
  protected $entityTypeManager;
  protected $eincidenciasManager;
  
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
    eincidenciasManagerInterface $eincidenciasManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->eincidenciasManager = $eincidenciasManager;
  }
  
  public function getDevelopmentsTable(array $developments) {
    $table = [];
    foreach ($developments as $value) {
      $url_development = Url::fromRoute('eincidencias.development', [
        'operation' => 'access',
        'node' => $value->id(),
      ]);
      $link_development = Link::fromTextAndUrl(t('Access'), $url_development);
      
      $table[$value->id()] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'description' => [
          '#markup' => $value->getTitle(),
        ],
        'access' => $link_development->toRenderable(),
      ];
    }
    
    return $table;
  }
  
  public function getCustomersTable(array $customers) {
    $table = [];
    foreach ($customers as $value) {
      $url_alteruser = Url::fromRoute('eincidencias.myprofile', [
        'user' => $value->id(),
      ]);
      $link_alteruser = Link::fromTextAndUrl(t('Access'), $url_alteruser);
      
      $url_dashboard = Url::fromRoute('eincidencias.dashboard', [
        'user' => $value->id(),
      ]);
      $link_dashboard = Link::fromTextAndUrl(t('Access'), $url_dashboard);
      
      $table[$value->id()] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'name' => [
          '#markup' => $value->get('field_eincidencias_name')->value,
        ],
        'mail' => [
          '#markup' => $value->getEmail(),
        ],
        'address' => [
          '#markup' => $value->get('field_eincidencias_address')->value,
        ],
        'alter' => $link_alteruser->toRenderable(),
        'access' => $link_dashboard->toRenderable(),
      ];
    }
    
    return $table;
  }
  
  public function getIncidentsNotAssignedTable(array $incidents) {
    $table = [];
    foreach ($incidents as $value) {
      $customer = $this->eincidenciasManager->getUser($value->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
      $development = $this->eincidenciasManager->getNodeEntity($customer->get('field_eincidencias_develop_id')->getValue()[0]['target_id']);
      $url_accessincident = Url::fromRoute('eincidencias.incident', [
        'operation' => 'access',
        'node' => $value->id(),
      ]);
      $link_accessincident = Link::fromTextAndUrl(t('Access'), $url_accessincident);
      $table[$value->id()] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'development' => [
          '#markup' => $development->getTitle(),
        ],
        'customer' => [
          '#markup' => $customer->get('field_eincidencias_name')->value,
        ],
        'address' => [
          '#markup' => $customer->get('field_eincidencias_address')->value,
        ],
        'date_creation' => [
          '#markup' => date('d/m/Y H:i', $value->get('created')->value),
        ],
        'access' => $link_accessincident->toRenderable(),
      ];
    }
    
    return $table;
  }
  
  public function getInterventionsIncidentsTable(array $interventions) {
    $table = [];
    foreach ($interventions as $value) {
      $outsource = $this->eincidenciasManager->getUser($value->get('field_eincidencias_outsource_id')->getValue()[0]['target_id']);
      $url_accessintervention = Url::fromRoute('eincidencias.intervention', [
        'operation' => 'access',
        'node' => $value->id(),
      ]);
      $link_accessintervention = Link::fromTextAndUrl(t('Access'), $url_accessintervention);
      $table[] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'description' => [
          '#markup' => $value->getTitle(),
        ],
        'outsource' => [
          '#markup' => $outsource->get('field_eincidencias_name')->value,
        ],
        'creation_date' => [
          '#markup' => date('d/m/Y - H:i', $value->get('created')->value),
        ],
        'access' => $link_accessintervention->toRenderable(),
      ];
    }
    return $table;
  }
  
  public function getInterventionsNotEnded(array $interventions) {
    $table = [];
    foreach ($interventions as $value) {
      $incident = $this->eincidenciasManager->getNodeEntity($value->get('field_eincidencias_incident_id')->getValue()[0]['target_id']);
      $customer = $this->eincidenciasManager->getUser($incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
      $url_accessintervention = Url::fromRoute('eincidencias.intervention', [
        'operation' => 'access',
        'node' => $value->id(),
      ]);
      $link_accessintervention = Link::fromTextAndUrl(t('Access'), $url_accessintervention);
      $table[$value->id()] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'description' => [
          '#markup' => $value->getTitle(),
        ],
        'date_creation' => [
          '#markup' => date('d/m/Y H:i', $value->get('created')->value),
        ],
        'customer' => [
          '#markup' => $customer->get('field_eincidencias_name')->value,
        ],
        'address' => [
          '#markup' => $customer->get('field_eincidencias_address')->value,
        ],
        'access' => $link_accessintervention->toRenderable(),
      ];
    }
    
    return $table;
  }
  
  public function getInterventionsEnded(array $interventions) {
    $table = [];
    foreach ($interventions as $value) {
      $incident = $this->eincidenciasManager->getNodeEntity($value->get('field_eincidencias_incident_id')->getValue()[0]['target_id']);
      $customer = $this->eincidenciasManager->getUser($incident->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
      $url_accessintervention = Url::fromRoute('eincidencias.intervention', [
        'operation' => 'access',
        'node' => $value->id(),
      ]);
      $link_accessintervention = Link::fromTextAndUrl(t('Access'), $url_accessintervention);
      
      $table[$value->id()] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'description' => [
          '#markup' => $value->getTitle(),
        ],
        'date_end' => [
          '#markup' => date('d/m/Y H:i', $value->get('field_eincidencias_date_end')->value),
        ],
        'customer' => [
          '#markup' => $customer->get('field_eincidencias_name')->value,
        ],
        'address' => [
          '#markup' => $customer->get('field_eincidencias_address')->value,
        ],
        'access' => $link_accessintervention->toRenderable(),
      ];
    }
    
    return $table;
  }
  
  public function getFilesTable(array $files) {
    $table = [];
    foreach($files as $value) {
      $file_uri = $value->getFileUri();
      $url = Url::fromUri(file_create_url($file_uri));
      $link = Link::fromTextAndUrl($value->getFilename(), $url);
      $table[] = [
        'file' => $link->toRenderable(),
      ];
    }
    
    return $table;
  }
}