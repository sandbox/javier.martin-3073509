<?php 
namespace Drupal\eincidencias;

interface eincidenciasFormatterInterface {
  public function getDevelopmentsTable(array $developments);
  public function getCustomersTable(array $customers);
  public function getIncidentsNotAssignedTable(array $incidents);
  public function getInterventionsIncidentsTable(array $interventions);
  public function getInterventionsNotEnded(array $interventions);
  public function getInterventionsEnded(array $interventions);
  public function getFilesTable(array $files);
}