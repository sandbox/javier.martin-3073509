<?php
namespace Drupal\eincidencias;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Exception;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class eincidenciasManager implements eincidenciasManagerInterface {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $fileUsage;
  protected $config;
  protected $mailManager;
  protected $languageManager;
  
  /**
   * Construct function
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user, 
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger,
      DatabaseFileUsageBackend $fileUsage, ConfigFactoryInterface $config, 
      MailManagerInterface $mailManager, LanguageManagerInterface $languageManager) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('eincidencias');
    $this->fileUsage = $fileUsage;
    $this->config = $config;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }
  
  /**
   * Create function
   * @param ContainerInterface $container
   * @return \Drupal\eincidencias\eincidenciasManager
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('file.usage'),
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::addDevelopment()
   */
  public function addDevelopment(array $params) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $development =  $node_storage->create([
      'type' => 'development',
      'title' => $params['title'],
      'body' => $params['body'],
      'status' => TRUE,
    ]);
    $development->save();
    
    // Log message
    $this->logger->info('@login - Development - Added development @title', [
      '@login' => $this->current_user->getUsername(),
      '@title' => $development->getTitle(),
    ]);
    
    return $development;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::updateDevelopment()
   */
  public function updateDevelopment(NodeInterface $development, array $params) {
    if (array_key_exists('title', $params))
      $development->get('title')->setValue($params['title']);
    if (array_key_exists('body', $params))
      $development->get('body')->setValue($params['body']);
    $development->save();
    
    // Log message
    $this->logger->info('@login - Development - Update development @title', [
      '@login' => $this->current_user->getUsername(),
      '@title' => $development->getTitle(),
    ]);
  }
  
  /**
   * Check if user exists
   * @param string $username
   * @return boolean
   */
  protected function checkUserExist(string $username) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $user = $user_storage->loadByProperties([
      'name' => $username,
    ]);
    return (empty($user)) ? FALSE : TRUE;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::addCustomer()
   */
  public function addCustomer(array $params) {
    $user = null;
    $user_storage = $this->entityTypeManager->getStorage('user');
    if (!$this->checkUserExist($params['name'])) {
      $user = $user_storage->create([
        'name' => $params['mail'],
        'mail' => $params['mail'],
        'pass' => $params['pass'],
        'field_eincidencias_name' => $params['field_eincidencias_name'],
        'field_eincidencias_phone' => $params['field_eincidencias_phone'],
        'field_eincidencias_address' => $params['field_eincidencias_address'],
        'field_eincidencias_develop_id' => $params['field_eincidencias_develop_id'],
        'status' => TRUE,
      ]);
      $user->addRole('eincidencias_customer');
      $user->save();
      
      // Send mail
      if ($this->config->get('eincidencias.settings')->get('notification_adduser') && array_key_exists('sendmail', $params) && $params['sendmail']) {
        $language_code = $this->languageManager->getDefaultLanguage()->getId();
        $params['customer'] = $user;
        $this->mailManager->mail('eincidencias', 'mail_adduser', $user->getEmail(), $language_code, $params, NULL, TRUE);
      }
      
      // Log message
      $this->logger->info('@login - Development - Added new customer @username', [
        '@login' => $this->current_user->getUsername(),
        '@username' => $user->getAccountName(),
      ]);
    } else {
      // Log message
      $this->logger->info('@login - Development - User @username not added', [
        '@login' => $this->current_user->getUsername(),
        '@username' => $params['mail'],
      ]);
    }
    
    return $user;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::updateCustomer()
   */
  public function updateCustomer(UserInterface $user, array $params) {
    if ($params['pass'] != '') {
      $user->setPassword($params['pass']);
    }
    if (array_key_exists('field_eincidencias_name', $params))
      $user->get('field_eincidencias_name')->setValue($params['field_eincidencias_name']);
      if (array_key_exists('field_eincidencias_phone', $params))
        $user->get('field_eincidencias_phone')->setValue($params['field_eincidencias_phone']);
        if (array_key_exists('field_eincidencias_address', $params))
          $user->get('field_eincidencias_address')->setValue($params['field_eincidencias_address']);
          $user->save();
          
          // Log message
          $this->logger->info('@login - Development - Update user @username', [
            '@login' => $this->current_user->getUsername(),
            '@username' => $user->getAccountName(),
          ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::addIncident()
   */
  public function addIncident(UserInterface $customer, array $params) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    foreach($params['files'] as $fid) {
      $file = $file_storage->load($fid);
      $this->fileUsage->add($file, 'eincidencias', 'user', 1);
    }
    $node_storage = $this->entityTypeManager->getStorage('node');
    $incident = $node_storage->create([
      'type' => 'incident',
      'status' => TRUE,
      'title' => 'Incident by user ' . $customer->getAccountName(),
      'body' => $params['body'],
      'field_eincidencias_files' => $params['files'],
      'field_eincidencias_customer_id' => $customer->id(),
      'field_eincidencias_technical_id' => $this->config->get('eincidencias.settings')->get('default_technician'),
    ]);
    $incident->save();
    
    // Send mail
    if ($this->config->get('eincidencias.settings')->get('notification_addincident')) {
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $params['incident'] = $incident;
      $params['customer'] = $customer;
      $params['technical'] = $this->getUser($this->config->get('eincidencias.settings')->get('default_technician'));
      $this->mailManager->mail('eincidencias', 'mail_addincident', $customer->getEmail(), $language_code, $params, NULL, TRUE);
    }
    
    // Log message
    $this->logger->info('@login - Incident - Added incident @title', [
      '@login' => $this->current_user->getUsername(),
      '@title' => $incident->getTitle(),
    ]);
    
    return $incident;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::updateIncident()
   */
  public function updateIncident(NodeInterface $incident, array $params) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    foreach($params['files'] as $fid) {
      $file = $file_storage->load($fid);
      $this->fileUsage->add($file, 'eincidencias', 'user', 1);
    }
    if (array_key_exists('body', $params))
      $incident->get('body')->setValue($params['body']);
      if (array_key_exists('files', $params))
        $incident->get('field_eincidencias_files')->setValue($params['files']);
        
        // Log message
        $this->logger->info('@login - Incident - Updated incident @title', [
          '@login' => $this->current_user->getUsername(),
          '@title' => $incident->getTitle(),
        ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::addIntervention()
   */
  public function addIntervention(array $params) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $intervention = $node_storage->create([
      'type' => 'intervention',
      'status' => TRUE,
      'title' => $params['title'],
      'field_eincidencias_incident_id' => $params['field_eincidencias_incident_id'],
      'field_eincidencias_outsource_id' => $params['field_eincidencias_outsource_id'],
      'field_eincidencias_date_end' => 0,
    ]);
    $intervention->save();
    
    // Send mail
    if ($this->config->get('eincidencias.settings')->get('notification_addintervention')) {
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $params['intervention'] = $intervention;
      $params['incident'] = $this->getNodeEntity($params['field_eincidencias_incident_id']);
      $params['customer'] = $this->getUser($params['incident']->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
      $params['technical'] = $this->getUser($params['incident']->get('field_eincidencias_technical_id')->getValue()[0]['target_id']);
      $params['outsource'] = $this->getUser($params['field_eincidencias_outsource_id']);
      $this->mailManager->mail('eincidencias', 'mail_addintervention', $params['outsource']->getEmail(), $language_code, $params, NULL, TRUE);
    }
    
    // Log message
    $this->logger->info('@login - Intervention - Added intervention @title', [
      '@login' => $this->current_user->getUsername(),
      '@title' => $intervention->getTitle(),
    ]);
    
    return $intervention;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::updateIntervention()
   */
  public function updateIntervention(NodeInterface $intervention, array $params) {
    if (array_key_exists('title', $params))
      $intervention->setTitle($params['title']);
      if (array_key_exists('field_eincidencias_date_end', $params))
        $intervention->get('field_eincidencias_date_end')->setValue($params['field_eincidencias_date_end']);
        $intervention->save();
        
        // Log message
        $this->logger->info('@login - Intervention - Update intervention @title', [
          '@login' => $this->current_user->getUsername(),
          '@title' => $intervention->getTitle(),
        ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::addMessage()
   */
  public function addMessage(array $params) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    foreach($params['files'] as $fid) {
      $file = $file_storage->load($fid);
      $this->fileUsage->add($file, 'eincidencias', 'user', 1);
    }
    $node_storage = $this->entityTypeManager->getStorage('node');
    $message = $node_storage->create([
      'type' => 'message',
      'status' => TRUE,
      'title' => $params['title'],
      'body' => $params['body'],
      'field_eincidencias_files' => $params['files'],
      'field_eincidencias_interven_id' => $params['field_eincidencias_interven_id'],
    ]);
    $message->save();
    
    // Send mail
    if ($this->config->get('eincidencias.settings')->get('notification_addmessage')) {
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $params['message'] = $message;
      $params['intervention'] = $this->getNodeEntity($params['field_eincidencias_interven_id']);
      $params['incident'] = $this->getNodeEntity($params['intervention']->get('field_eincidencias_incident_id')->getValue()[0]['target_id']);
      $params['customer'] = $this->getUser($params['incident']->get('field_eincidencias_customer_id')->getValue()[0]['target_id']);
      $params['technical'] = $this->getUser($params['incident']->get('field_eincidencias_technical_id')->getValue()[0]['target_id']);
      $params['outsource'] = $this->getUser($params['intervention']->get('field_eincidencias_outsource_id')->getValue()[0]['target_id']);
      $this->mailManager->mail('eincidencias', 'mail_addmessage', $params['outsource']->getEmail(), $language_code, $params, NULL, TRUE);
      $this->mailManager->mail('eincidencias', 'mail_addmessage', $params['customer']->getEmail(), $language_code, $params, NULL, TRUE);
    }
    
    // Log message
    $this->logger->info('@login - Message - Added message @title', [
      '@login' => $this->current_user->getUsername(),
      '@title' => $message->getTitle(),
    ]);
    
    return $message;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::updateMessage()
   */
  public function updateMessage(NodeInterface $message, array $params) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    foreach($params['files'] as $fid) {
      $file = $file_storage->load($fid);
      $this->fileUsage->add($file, 'eincidencias', 'user', 1);
    }
    
    if (array_key_exists('title', $params))
      $message->setTitle($params['title']);
      if (array_key_exists('body', $params))
        $message->get('body')->setValue($params['body']);
        $message->save();
        
        // Log message
        $this->logger->info('@login - Message - Updated message @title', [
          '@login' => $this->current_user->getUsername(),
          '@title' => $message->getTitle(),
        ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getDevelopmentCustomers()
   */
  public function getDevelopmentCustomers(NodeInterface $development) {
    $storage_user = $this->entityTypeManager->getStorage('user');
    $users = $storage_user->loadByProperties([
      'status' => TRUE,
      'field_eincidencias_develop_id' => $development->id(),
    ]);
    
    return $users;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getDevelopmentIncidentsNotAssigned()
   */
  public function getDevelopmentIncidentsNotAssigned(NodeInterface $development) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $users = $user_storage->getQuery()
    ->condition('field_eincidencias_develop_id', $development->id(), '=')
    ->condition('status', TRUE, '=')
    ->execute();
    
    $interventions = $node_storage->getQuery()
    ->condition('type', 'intervention', '=')
    ->condition('status', TRUE, '=')
    ->execute();
    
    $incidents_inter_id = [];
    foreach ($node_storage->loadMultiple($interventions) as $value) {
      $incidents_inter_id[] = $value->get('field_eincidencias_incident_id')->getValue()[0]['target_id'];
    }
    
    try {
      $incidents_id = $node_storage->getQuery()
      ->condition('type', 'incident', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_customer_id', $users, 'in')
      ->execute();
      $incidents = $node_storage->loadMultiple($incidents_id);
      foreach ($incidents as $key => $value) {
        if (in_array($incidents[$key]->id(), $incidents_inter_id)) {
          unset($incidents[$key]);
        }
      }
    } catch (Exception $e) {
      $incidents = [];
    }
    
    return $incidents;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getDevelopmentInterventionsNotEnded()
   */
  public function getDevelopmentInterventionsNotEnded(NodeInterface $development) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $users = $user_storage->getQuery()
    ->condition('field_eincidencias_develop_id', $development->id(), '=')
    ->condition('status', TRUE, '=')
    ->execute();
    try {
      $incidents = $node_storage->getQuery()
      ->condition('type', 'incident', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_customer_id', $users, 'in')
      ->execute();
      
      $interventions = $node_storage->getQuery()
      ->condition('type', 'intervention', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_incident_id', $incidents, 'in')
      ->condition('field_eincidencias_date_end', 0, '=')
      ->execute();
    } catch (Exception $e) {
      $interventions = [];
    }
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getDevelopmentInterventionsEnded()
   */
  public function getDevelopmentInterventionsEnded(NodeInterface $development) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $users = $user_storage->getQuery()
    ->condition('field_eincidencias_develop_id', $development->id(), '=')
    ->condition('status', TRUE, '=')
    ->execute();
    
    try {
      $incidents = $node_storage->getQuery()
      ->condition('type', 'incident', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_customer_id', $users, 'in')
      ->execute();
      
      $interventions = $node_storage->getQuery()
      ->condition('type', 'intervention', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_incident_id', $incidents, 'in')
      ->condition('field_eincidencias_date_end', 0, '!=')
      ->execute();
    } catch (Exception $e) {
      $interventions = [];
    }
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getDevelopments()
   */
  public function getDevelopments() {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    return $node_storage->loadByProperties([
      'type' => 'development',
      'status' => TRUE,
    ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getInterventionIncident()
   */
  public function getInterventionIncident(NodeInterface $intervention) {
    $incident_id = $intervention->get('field_eincidencias_incident_id')->getValue()[0]['target_id'];
    return $this->entityTypeManager->getStorage('node')->load($incident_id);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getInterventionMessages()
   */
  public function getInterventionMessages(NodeInterface $intervention) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $messages = $node_storage->loadByProperties([
      'type' => 'message',
      'status' => TRUE,
      'field_eincidencias_interven_id' => $intervention->id(),
    ]);
    
    return $messages;
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::isInterventionFinished()
   */
  public function isInterventionFinished(NodeInterface $intervention) {
    if ($intervention->getType() == 'intervention') {
      return ($intervention->get('field_eincidencias_date_end')->value == 0) ? FALSE : TRUE;
    } else {
      return FALSE;
    }
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getMessageIntervention()
   */
  public function getMessageIntervention(NodeInterface $message) {
    $intervention_id = $message->get('field_eincidencias_intervention_id')->getValue()[0]['target_id'];
    return $this->entityTypeManager->getStorage('node')->load($intervention_id);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::emptyConfigDefaultTechnician()
   */
  public function emptyConfigDefaultTechnician() {
    return empty($this->config->get('eincidencias.settings')->get('default_technician'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getUsers()
   */
  public function getUsers(string $role) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $result = $user_storage->getQuery()
    ->condition('roles', $role, '=')
    ->condition('status', TRUE, '=')
    ->execute();
    
    return $user_storage->loadMultiple($result);
  }
    
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getUser()
   */
  public function getUser(int $uid) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $user = $user_storage->load($uid);
    
    return $user;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getFiles()
   */
  public function getFiles(NodeInterface $node) {
    $files = [];
    try {
      foreach($node->get('field_eincidencias_files')->getValue() as $value) {
        $files = $value['target_id'];
      }
    } catch (Exception $e) {
      $this->logger->error('Entity without field_eincidencias_files field');
    }
   
    return $files;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getNodeEntity()
   */
  public function getNodeEntity(int $nid) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    return $node_storage->load($nid);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getIncidentInterventions()
   */
  public function getIncidentInterventions(NodeInterface $incident) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    return $node_storage->loadByProperties([
      'type' => 'intervention',
      'status' => TRUE,
      'field_eincidencias_incident_id' => $incident->id(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getFilesEntity()
   */
  public function getFilesEntity(NodeInterface $node) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    $table = [];
    try {
      $files = $node->get('field_eincidencias_files')->getValue();
      foreach ($files as $value) {
        $table[] = $file_storage->load($value['target_id']);
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getTechnicalIncidentNotAssigned()
   */  
  public function getTechnicalIncidentNotAssigned(UserInterface $technician) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $interventions_id = $node_storage->getQuery()
    ->condition('type', 'intervention', '=')
    ->condition('status', TRUE, '=')
    ->execute();
    
    $incidents_inter_id = [];
    foreach ($node_storage->loadMultiple($interventions_id) as $value) {
      $incidents_inter_id[] = $value->get('field_eincidencias_incident_id')->getValue()[0]['target_id'];
    }
    
    try {
      $incidents_id = $node_storage->getQuery()
      ->condition('type', 'incident', '=')
      ->condition('status', TRUE, '=')
      ->condition('field_eincidencias_technical_id', $technician->id(), 'in')
      ->execute();
      $incidents = $node_storage->loadMultiple($incidents_id);
      foreach ($incidents as $key => $value) {
        if (in_array($incidents[$key]->id(), $incidents_inter_id)) {
          unset($incidents[$key]);
        }
      }
    } catch (Exception $e) {
      $incidents = [];
    }
    
    return $incidents;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getTechnicalInterventionsNotEnded()
   */
  public function getTechnicalInterventionsNotEnded(UserInterface $technician) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $incidents = $node_storage->getQuery()
    ->condition('type', 'incident', '=')
    ->condition('status', TRUE, '=')
    ->condition('field_eincidencias_technical_id', $technician->id(), '=')
    ->execute();
    
    try {
      $interventions = $node_storage->getQuery()
      ->condition('type', 'intervention', '=')
      ->condition('status', true, '=')
      ->condition('field_eincidencias_incident_id', $incidents, 'in')
      ->condition('field_eincidencias_date_end', 0, '=')
      ->execute();
    } catch (Exception $e) {
      $interventions = [];
    }
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getCustomerInterventionsNotEnded()
   */
  public function getCustomerInterventionsNotEnded(UserInterface $customer) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $incidents = $node_storage->getQuery()
    ->condition('type', 'incident', '=')
    ->condition('status', TRUE, '=')
    ->condition('field_eincidencias_customer_id', $customer->id(), '=')
    ->execute();
    try {
      $interventions = $node_storage->getQuery()
      ->condition('type', 'intervention', '=')
      ->condition('status', true, '=')
      ->condition('field_eincidencias_incident_id', $incidents, 'in')
      ->condition('field_eincidencias_date_end', 0, '=')
      ->execute();
    } catch (Exception $e) {
      $interventions = [];
    }
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getCustomerInterventionsEnded()
   */
  public function getCustomerInterventionsEnded(UserInterface $customer) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $incidents = $node_storage->getQuery()
    ->condition('type', 'incident', '=')
    ->condition('status', TRUE, '=')
    ->condition('field_eincidencias_customer_id', $customer->id(), '=')
    ->execute();
    
    try {
      $interventions = $node_storage->getQuery()
      ->condition('type', 'intervention', '=')
      ->condition('status', true, '=')
      ->condition('field_eincidencias_incident_id', $incidents, 'in')
      ->condition('field_eincidencias_date_end',0, '!=')
      ->execute();
    } catch (Exception $e) {
      $interventions = [];
    }
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getCustomerIncidents()
   */
  public function getCustomerIncidents(UserInterface $customer) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $incidents = $node_storage->getQuery()
    ->condition('type', 'incident', '=')
    ->condition('status', TRUE, '=')
    ->condition('field_eincidencias_customer_id', $customer->id(), '=')
    ->execute();
    
    return $node_storage->loadMultiple($incidents);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getOutsourceInterventionsNotEnded()
   */
  public function getOutsourceInterventionsNotEnded(UserInterface $outsource) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $interventions = $node_storage->getQuery()
    ->condition('type', 'intervention', '=')
    ->condition('status', true, '=')
    ->condition('field_eincidencias_outsource_id', $outsource->id(), '=')
    ->condition('field_eincidencias_date_end', 0, '=')
    ->execute();
    
    return $node_storage->loadMultiple($interventions);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\eincidencias\eincidenciasManagerInterface::getOutsourceInterventionsEnded()
   */
  public function getOutsourceInterventionsEnded(UserInterface $outsource) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    $interventions = $node_storage->getQuery()
    ->condition('type', 'intervention', '=')
    ->condition('status', true, '=')
    ->condition('field_eincidencias_outsource_id', $outsource->id(), '=')
    ->condition('field_eincidencias_date_end', 0, '!=')
    ->execute();
    
    return $node_storage->loadMultiple($interventions);
  }
  
}