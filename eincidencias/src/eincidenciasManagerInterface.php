<?php
namespace Drupal\eincidencias;

use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

interface eincidenciasManagerInterface {
  /**
   * Add development.
   * @param array $params
   */
  public function addDevelopment(array $params);
  
  /**
   * Update development.
   * @param NodeInterface $development
   * @param array $params
   */
  public function updateDevelopment(NodeInterface $development, array $params);
  
  /**
   * Add customer.
   * @param array $params
   */
  public function addCustomer(array $params);
  
  /**
   * Update customer.
   * @param UserInterface $user
   * @param array $params
   */
  public function updateCustomer(UserInterface $user, array $params);
  
  /**
   * Add new incident
   * @param UserInterface $customer
   */
  public function addIncident(UserInterface $customer, array $params);
  
  /**
   * Update incident
   * @param NodeInterface $incident
   * @param array $params
   */
  public function updateIncident(NodeInterface $incident, array $params);
  
  /**
   * Add intervention
   * @param array $params
   */
  public function addIntervention(array $params);
  
  /**
   * Update intervention
   * @param NodeInterface $intervention
   * @param array $params
   */
  public function updateIntervention(NodeInterface $intervention, array $params);
  
  /**
   * Add message
   * @param array $params
   */
  public function addMessage(array $params);
  
  /**
   * Update message
   * @param NodeInterface $message
   * @param array $params
   */
  public function updateMessage(NodeInterface $message, array $params);
  
  /**
   * Return all customer from a development.
   * @param NodeInterface $development
   */
  public function getDevelopmentCustomers(NodeInterface $development);
  
  /**
   * Return all incidents not assigned (with no interventions) from a development
   * @param NodeInterface $development
   */
  public function getDevelopmentIncidentsNotAssigned(NodeInterface $development);
  
  /**
   * Return all interventions not ended for a development
   * @param NodeInterface $development
   */
  public function getDevelopmentInterventionsNotEnded(NodeInterface $development);
  
  /**
   * Return all interventions ended for a development
   * @param NodeInterface $development
   */
  public function getDevelopmentInterventionsEnded(NodeInterface $development);
  
  /**
   * Return all developments
   */
  public function getDevelopments();
  
  /**
   * Return all interventions for a incident
   * @param NodeInterface $incident
   */
  public function getIncidentInterventions(NodeInterface $incident);
  
  /**
   * Return all incidents for a intervention
   * @param NodeInterface $intervention
   */
  public function getInterventionIncident(NodeInterface $intervention);
  
  /**
   * Return all messages for a intervention
   * @param NodeInterface $intervention
   */
  public function getInterventionMessages(NodeInterface $intervention);
  
  /**
   * Return true if intervention is finished
   * @param NodeInterface $intervention
   */
  public function isInterventionFinished(NodeInterface $intervention);
  
  /**
   * Return intervertion that message belongs.
   * @param NodeInterface $message
   */
  public function getMessageIntervention(NodeInterface $message);
  
  /**
   * Check if default technician is set on module configuration
   */
  public function emptyConfigDefaultTechnician();
  
  /**
   * Return all users for a role
   * @param string $role
   */
  public function getUsers(string $role);
  
  /**
   * Return user for this uid.
   * @param int $uid
   */
  public function getUser(int $uid);
  
  /**
   * Return array with files_eincidencias_files fid's that node belongs 
   * @param NodeInterface $node
   */
  public function getFiles(NodeInterface $node);
  
  /**
   * Return node entity for a nid
   * @param int $nid
   */
  public function getNodeEntity(int $nid);
  
  /**
   * Return all files entities from field_eincidencias_files that node belongs
   * @param NodeInterface $node
   */
  public function getFilesEntity(NodeInterface $node);
  
  /**
   * Return incidents not assigned (with no interventions) for a technician
   * @param UserInterface $user
   */
  public function getTechnicalIncidentNotAssigned(UserInterface $technician);
  
  /**
   * Return interventions not ended for a technician 
   * @param UserInterface $user
   */
  public function getTechnicalInterventionsNotEnded(UserInterface $technician);
  
  /**
   * Return interventions not ended for a customer
   * @param UserInterface $user
   */
  public function getCustomerInterventionsNotEnded(UserInterface $customer);
  
  /**
   * Return interventions ended for a customer
   * @param UserInterface $user
   */
  public function getCustomerInterventionsEnded(UserInterface $customer);
  
  /**
   * Return all incidents for a customer
   * @param UserInterface $customer
   */
  public function getCustomerIncidents(UserInterface $customer);
  
  /**
   * Return interventions not ended for a customer
   * @param UserInterface $outsource
   */
  public function getOutsourceInterventionsNotEnded(UserInterface $outsource);
  
  /**
   * Return interventions ended for a customer
   * @param UserInterface $outsource
   */
  public function getOutsourceInterventionsEnded(UserInterface $outsource);
}